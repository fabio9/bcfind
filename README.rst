Brain cell finder
=================

Brain cell finder is a tool for fully automated localization of soma
in 3D mouse brain images acquired by confocal light sheet microscopy.

For documentation, please see the 
`bcfind page at DINFO, Università di Firenze <http://bcfind.dinfo.unifi.it>`_

The method is detailed in the paper:

Paolo Frasconi, Ludovico Silvestri, Paolo Soda, Roberto Cortini, Francesco Pavone, and Giulio Iannello. 
Large-scale automated identification of mouse brain cells in confocal light sheet microscopy images.
*Bioinformatics*, 2014. To Appear.
